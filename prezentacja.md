---
author: Kacper Giemza
title: Michael Jackson
subtitle: 
date: 14.03.2021
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Kim był Michael Jackson?

Michael Jackson był amerykańskim piosenkarzem muzyki pop, soul, rock i funk, autor tekstów, artysta estradowy, 	tancerz, aktor, kompozytor i filantrop, którego kariera i życie osobiste stały się ważną częścią kultury masowej. 	Zaliczany do najwybitniejszych wykonawców i kompozytorów oraz najpopularniejszych artystów w historii, a ze względu na swoje osiągnięcia w branży muzycznej nazywany „Królem Popu”.
![](mj.jpg)

## Dzieciństwo i początki kariery

Swoją karierę zaczynał w zespole The Jackson 5 wraz z czterema braćmi. Niespełna jedenastoletni Michael był gwiazdą zespołu. Na zdjęciu Michael pośrodku.
![](mj2.jpg){height=50% width=50%}

## Pierwszy album

W 1978 roku Michael zadebiutował jako aktor w filmie muzycznym „The Wiz”. Rok później wytwórnia Epic wydała pierwszy solowy album dorosłego już Micheala – „ Off The Wall”.
![](mj5.jpg){height=50% width=50%}

## „Moonwalk” 

![](mj8.jpg){height=50% width=50%}
Sukcesem okazała się również kolejna płyta Jacksona „Thriller”, która rozeszła się w liczbie 51 milionów egzemplarzy na całym świecie. Na potrzeby albumu artysta stworzył swój słynny krok taneczny „Moonwalk”, za który podobno otrzymał gratulacje od samego Freda Astaire ‘a.

## „Bad”

W 1987 roku Jackson wydał album „Bad” ,którego trwająca 2 lata produkcja pochłonęła 2 miliony dolarów. 
![](mj6.jpg){height=50% width=50%}

## Jackson w Polsce

![](mj11.jpg){height=50% width=50%}
W 1995roku artysta wydał kolejna płytę „History”. W ramach jej promocji Jackson m.in. odwiedził Polskę (1996). Jego Warszawski koncert obejrzało 120 tys. widzów. W 1997 roku Jackson zadeklarował chęć budowy parku rozrywki w Warszawie, ponieważ Polska, jak powiedział: „Jest jednym z najbardziej inspirujących krajów, jakie w życiu odwiedził”.

## „Artysta Millenium” 

W 2000 roku w Monaco Jackson odebrał specjalną „Światową Nagrodę Muzyczna – „Artysta Millenium”. Rok później wydał swój ostatni solowy album „Invincible”. Michael Jackson Zmarł 25 czerwca 2009 roku w Los Angeles. Swoje kondolencje przesyłały gwiazdy z całego świata. Był najbardziej uhonorowanym artysta w historii. W trakcie swojej 40-letniej kariery otrzymał liczne tytuły i nagrody.
![](mj13.jpg){height=50% width=50%}

## DZIĘKUJĘ ZA UWAGĘ
![](mj14.jpg)
